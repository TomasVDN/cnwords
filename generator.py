import os
import time
import shutil
import random

chapter = None
possibleChapters = ["1","2","3","4","5","6","7","8","9","quote"] #change this list for other books

def main():
	action = None
	while (action != "exit"):
		os.system('cls' if os.name == 'nt' else 'clear')
		action = input("Next action?\n")
		if action == "add":
			addWord()
		if action == "list":
			listAll()
		if action == "duke nukem":
			removeAll()
		if action == "yolo":
			askQuestions()
		if action == "help":
			print("add - adds an entry to the database.\nlist - lists all the registered words.\nduke nukem - empties databank. Use at own risk.\nyolo - asks the definition of a random word.\nhelp - prints this help\nexit - quit programm\n\n(press enter to continue)")
			input()
	return
		

def addWord():
	os.system('cls' if os.name == 'nt' else 'clear')
	
	chapter = input("Please, enter chapter: ")
	while (chapter not in possibleChapters):
		chapter = input("Please, enter chapter: ")
		
	word = input("Please, enter word: ")
	definition = input("Please, enter definition: ")
	
	os.system('cls' if os.name == 'nt' else 'clear')
	
	print("Adding word to database")
	
	path = "data/" + chapter + "/" + word + ".txt"

	file = open(path, 'w')
	file.write(word + '\n' + definition + '\n')
	file.close
	
	index = open("index.txt", "a")
	index.write(word + "\n")
	index.close
	
	time.sleep(1)
	print("Word added to database")
	time.sleep(1.5)
	os.system('cls' if os.name == 'nt' else 'clear')
	
def listAll():
	os.system('cls' if os.name == 'nt' else 'clear')
	file = open("index.txt", "r")
	index=file.readlines()
	index.sort()
	for i in range(len(index)):
		print(index[i])
		
	input("(Press enter to return)\n")
	
def removeAll():
	response = input("destroy all evidence? (yes/no)")
	
	if (response == "yes"):
		shutil.rmtree("data")
		os.remove("index.txt")
	
		os.mkdir("data")
		for i in possibleChapters:
			path = "data/" + i
			os.mkdir(path)

		file = open("index.txt","w")
		file.close()
		
def askQuestions():
	os.system('cls' if os.name == 'nt' else 'clear')
	chapter = input("From which chapter do you want questions?\n")
	while (chapter not in possibleChapters):
		chapter = input("From which chapter do you want questions?\n")
		
	fol = None
	
	while fol != "stop":
		os.system('cls' if os.name == 'nt' else 'clear')
		random_file = random.choice(os.listdir("data/" + chapter))
		if random_file == None:
			return
		
		path = "data/" + chapter + "/" + random_file
		file = open(path, "r")
		
		input("What can you say about: " + file.readline() + "\n(press enter for answer)")
		fol = input("\n" + file.readline() + "\n(press enter to continue, type stop to stop)")
		
		file.close()
		
		

main()
